# Irisdown Countdown Timer : Remote LED Display for Olimex ESP32-POE / ESP32-GATEWAY#

This is an Arduino sketch for the Olimex ESP32-POE / ESP32-GATEWAY to show a countdown from Irisdown Countdown Timer https://www.irisdown.co.uk/countdowntimer.html

### IDE setup ###

* Download and install Arduino IDE from https://www.arduino.cc/en/main/software
* In Preferences, Settings, Additional Board Manager URLs, enter 'https://espressif.github.io/arduino-esp32/package_esp32_index.json'
* Open Tools, Board, Boards Manager and install esp32 by Espressif Systems (latest version!)
* Download Adafruit_LED_Backpack library from https://github.com/adafruit/Adafruit_LED_Backpack and place the folder in your arduino_sketch_folder/libraries folder
* Repeat with Adafruit_GXF_Library from https://github.com/adafruit/Adafruit-GFX-Library
* Open Tools, Manage Libraries and ensure these two libraries are installed
* Download the ESP-32_POE_-_Irisdown_Countdown_Remote folder to your arduino_sketch_folder and open the sketch
* Set your Board in Tools, Board to Olimex ESP32-POE, ESP32-POE-ISO or ESP32-GATEWAY
* Compile and upload with a micro USB cable

### Hardware requirement ###

* 1 x Olimex ESP32-POE https://www.olimex.com/Products/IoT/ESP32/ESP32-POE/open-source-hardware , ESP32-POE-ISO https://www.olimex.com/Products/IoT/ESP32/ESP32-POE-ISO/open-source-hardware or ESP32-GATEWAY https://www.olimex.com/Products/IoT/ESP32/ESP32-GATEWAY/open-source-hardware
* 1 x Adafruit 4-Digit 7-Segment Display w/I2C Backpack https://www.adafruit.com/product/1002 or similar
* 1 x POE injector / POE switch for powering POE boards

### ESP32-POE / ESP32-POE-ISO Wiring ###

ESP32-POE-ISO schematic https://github.com/OLIMEX/ESP32-POE-ISO/blob/master/HARDWARE/ESP32-PoE-ISO-Rev.C/ESP32-PoE-ISO_Rev_C.pdf

ESP32-POE schematic https://github.com/OLIMEX/ESP32-POE/raw/master/HARDWARE/ESP32-PoE-hardware-revision-C/ESP32-PoE_Rev_C_Color.pdf

Connect 4 or 5 pins like so:

| ESP32-POE      | LED Display |
|----------------|-------------|
| +5V            | VCC         |
| GND            | GND         |
| GPIO13/I2C-SDA | SDA         |
| GPIO16/I2C-SCL | SCL         |
| +3.3V          | IO          |

Some LED Displays will be missing the IO pin, so just omit that.

### ESP32-GATEWAY ###

ESP32-GATEWAY schematic https://github.com/OLIMEX/ESP32-GATEWAY/raw/master/HARDWARE/Hardware%20revision%20E/ESP32-GATEWAY_Rev_E_color.pdf

* Change line 32 of sketch to read #define ETH_PHY_POWER 5
* Change line 206 of Adafruit_LEDBackpack.cpp to Wire.begin(32,16) and use GPIO32 for SDA as default pin 17 is used for Ethernet

Connect 4 or 5 pins like so:

| ESP32-GATEWAY  | LED Display |
|----------------|-------------|
| +5V            | VCC         |
| GND            | GND         |
| GPIO32         | SDA         |
| GPIO16         | SCL         |
| +3.3V          | IO          |

Some LED Displays will be missing the IO pin, so just omit that.

### Demonstration ###

https://www.youtube.com/watch?v=o4B-W1Qq7ek


